import React from 'react';

class Board extends React.Component {

    renderSquare(i) {
        return <Square
                       onClick={() => this.props.onSquareClick(i)} 
                       value={this.props.squares[i]}
                       disabled={this.props.disableSquare}/>;
    }

    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

/**
 * Square is wrtitten as a function instead of 
 * a component class since it does not contain any state
 */
function Square(props) {
    return (
        <button className="square"
            onClick={props.onClick}
            disabled={props.disabled}>
            {props.value}
        </button>
    );
}

export default Board;