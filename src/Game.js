import React from 'react';
import Board from './Board';

class Game extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            squares: Array(9).fill(null),
            xIsNext: true,
            winner: null,
        };
    }

    render() {
        let status = (this.state.winner  == null? ('Next player: ' + (this.state.xIsNext ? 'X' : 'O')) : 'Player ' + this.state.winner + ' WINS!');

        return (
            <div className="game">
                <div className="game-board">
                    <Board onSquareClick={(i) => this.handleClick(i)}
                           squares={this.state.squares}
                           disableSquare={this.state.winner != null}/>
                </div>
                <div className="game-info">
                    <div>{ status }</div>
                    <ol>{/* TODO */}</ol>
                </div>
            </div>
        );
    }

    handleClick(i) {
        const slicedSquares = this.state.squares.slice();
        
        // don't change existing square value 
        if(slicedSquares[i] === null){
            slicedSquares[i] = this.state.xIsNext ? 'X' : 'O';
            this.setState({
                squares: slicedSquares,
                xIsNext: !this.state.xIsNext
            }, () => this.decideWinner());
        }
    }

    decideWinner() {
        const winningPattern = [[0, 1, 2],
                                [1, 4, 7],
                                [3, 4, 5],
                                [6, 7, 8],
                                [0, 4, 8],
                                [0, 3, 6],
                                [2, 4, 6],
                                [2, 5, 8]];

        winningPattern.forEach( pattern => {
            if (this.state.squares[pattern[0]] != null && this.state.squares[pattern[1]] != null && this.state.squares[pattern[2]] != null) {
                if (this.state.squares[pattern[0]] === this.state.squares[pattern[1]]
                    && this.state.squares[pattern[0]] === this.state.squares[pattern[2]]) {
                    this.setState({ winner: this.state.squares[pattern[0]] });
                }
            }
        });

    }
}

export default Game;